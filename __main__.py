import csv
from dataclasses import dataclass
from pathlib import Path
from typing import Generator
from geojson import Polygon, GeometryCollection, FeatureCollection, Feature
from pyproj import Transformer
import json 

polys = []

@dataclass(frozen=True)
class Service:
    X: float
    Y: float
    lat: float
    lon: float

transformer_2154_to_4326 = Transformer.from_crs(2154, 4326)

def transfor_lambert_to_wgs(*, x: float, y: float) -> tuple[float, float]:
    return transformer_2154_to_4326.transform(x, y)[1], transformer_2154_to_4326.transform(x, y)[0]


def get_service_rows() -> Generator[Service, None, None]:
    with Path(__file__).parent.joinpath("bpe20_ensemble_xy.csv").open("r") as f:
        reader = csv.DictReader(f, delimiter=";")
        for row in reader:
            if (row['TYPEQU'][0] == 'D' and len(row["LAMBERT_X"]) 
                and len(row["LAMBERT_Y"]) and str(row['REG']) not in ['01', '02', '03', '04', '06', '94']):
                point = transfor_lambert_to_wgs(
                        x=float(row["LAMBERT_X"]),
                        y=float(row["LAMBERT_Y"])
                    )
                yield Service(
                    X = float(row["LAMBERT_X"]),
                    Y = float(row["LAMBERT_Y"]),
                    lat = point[1],
                    lon = point[0]
                )

def get_index_matrix(x: float, y: float) -> [int, int]:    
    return [int((x - 30_000)/15_000), int((7_160_000 - y)/15_000)]


def route_to_feature(idx, poly):
    return {
        'type': 'Feature',
        'geometry': poly,
        'properties': {
            'name': f'Route #{idx}'
        }
    }
    
def gen_geojson(i: int, j: int):
    global polys

    poly = Polygon([(
            transfor_lambert_to_wgs(x=(15_000*(j))+30_000, y=-(15_000*i)+7_160_000),
            transfor_lambert_to_wgs(x=(15_000*(j+1))+30_000, y=-(15_000*(i))+7_160_000), 
            transfor_lambert_to_wgs(x=(15_000*(j+1))+30_000, y=-(15_000*(i+1))+7_160_000),
            transfor_lambert_to_wgs(x=(15_000*j)+30_000, y=-(15_000*(i+1))+7_160_000),
            transfor_lambert_to_wgs(x=(15_000*(j))+30_000, y=-(15_000*i)+7_160_000)
        )])

    polys.append(poly)

if __name__ == "__main__":
    n = 0
    service_iter = get_service_rows()
    while True:
        try:
            if n < 4_000 :
                data = next(service_iter)
                print(data)
                indices = get_index_matrix(data.X, data.Y)
                print(indices)
                gen_geojson(indices[0], indices[1])
            else:
                break
            n += 1
        except StopIteration:
            break

    gc = Feature(polys)
    collection = FeatureCollection([
        route_to_feature(i, gc)
        for i, gc
        in enumerate(polys)
    ])

    with open('data.json', 'w') as f:
        json.dump(collection, f) 