import csv
import json
from dataclasses import dataclass
from pathlib import Path
from typing import Generator

import numpy
import pyspark
from geojson import Feature, FeatureCollection, GeometryCollection, Polygon
from pyproj import Transformer
from pyspark.sql import SparkSession

conf = (
    pyspark.SparkConf()
    .setMaster("local[*]")
    .setAll(
        [
            ("spark.executor.memory", "1g"),  # find
            ("spark.driver.memory", "1g"),  # your
            ("spark.driver.maxResultSize", "1g"),  # setup
        ]
    )
)
# create the session
spark = SparkSession.builder.config(conf=conf).getOrCreate()

# create the context
sc = spark.sparkContext
spark

services_data = (
    spark.read.option("delimiter", ";").csv("bpe20_ensemble_xy.csv", header=True).rdd
)

services_coordinates = services_data.filter(
    lambda line: line.TYPEQU[0] == "D"
    and line.REG not in ["01", "02", "03", "04", "06", "94"]
).map(lambda line: (line.LAMBERT_X, line.LAMBERT_Y))

matrix_data = (
    services_coordinates.filter(
        lambda line: line[0] is not None and line[1] is not None
    )
    .map(
        lambda line: (
            (
                int((float(line[0]) - 30_000) / 15_000),
                int((7_160_000 - float(line[1])) / 15_000),
            ),
            1,
        )
    )
    .reduceByKey(lambda a, b: a + b)
    .collect()
)

matrix = numpy.zeros((64, 75))

for cell in matrix_data:
    if cell[0][0] <= 63 and cell[0][1] <= 74:
        matrix[cell[0][0], cell[0][1]] = cell[1]

polys = []

transformer_2154_to_4326 = Transformer.from_crs(2154, 4326)


def transfor_lambert_to_wgs(*, x: float, y: float) -> tuple[float, float]:
    return (
        transformer_2154_to_4326.transform(x, y)[1],
        transformer_2154_to_4326.transform(x, y)[0],
    )


def route_to_feature(poly):
    return {"type": "Feature", "geometry": poly, "properties": {}}


def gen_geojson(i: int, j: int):
    global polys

    poly = Polygon(
        [
            (
                transfor_lambert_to_wgs(
                    x=(15_000 * (j)) + 30_000, y=-(15_000 * i) + 7_160_000
                ),
                transfor_lambert_to_wgs(
                    x=(15_000 * (j + 1)) + 30_000, y=-(15_000 * (i)) + 7_160_000
                ),
                transfor_lambert_to_wgs(
                    x=(15_000 * (j + 1)) + 30_000, y=-(15_000 * (i + 1)) + 7_160_000
                ),
                transfor_lambert_to_wgs(
                    x=(15_000 * j) + 30_000, y=-(15_000 * (i + 1)) + 7_160_000
                ),
                transfor_lambert_to_wgs(
                    x=(15_000 * (j)) + 30_000, y=-(15_000 * i) + 7_160_000
                ),
            )
        ]
    )

    polys.append(poly)


for i in range(64):
    for j in range(75):
        if matrix[i][j] != 0:
            gen_geojson(j, i)

    gc = Feature(polys)
    collection = FeatureCollection([route_to_feature(gc) for gc in enumerate(polys)])

    with open("data.json", "w") as f:
        json.dump(collection, f)
