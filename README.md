# KANT B2 : Les déserts médicaux avec Spark

*Kévin DETHOOR, Andrea CHAVEZ HERREJON, Nathan LE BOUDEC & Thibault CAMU*

### Préparation

Dans le répertoire racine du projet desert_medicaux_spark :

* wget https://www.insee.fr/fr/statistiques/fichier/3568638/bpe20_ensemble_xy_csv.zip
* unzip bpe20_ensemble_xy_csv.zip
